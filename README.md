# Конфигурация для Desty

Устанавливаем и активируем виртуальное окружение

```
#!bash
virtualenv -p /usr/bin/python3 .env
. .env/bin/activate
```

Устанавливаем зависимости virtualenv:

```
#!bash
pip install -r requirements.txt
```

Запускаем локальный сервер

```
#!bash
./manage.py runserver
```

И вот оно счастье -> http://127.0.0.1:8000