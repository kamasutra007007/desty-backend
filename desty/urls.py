from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', 'rest_framework_jwt.views.obtain_jwt_token'),
]
